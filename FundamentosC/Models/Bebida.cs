﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Clases_Objetos_Constructores.Models
{
    public class Bebida
    {
        public Bebida(string Nombre, int Cantidad)
        { 
            this.nombre= Nombre;
            this.cantidad = Cantidad;
        }
        
        public int cantidad { get; set; }
        public string nombre { get; set; }
        
        public void beberse( int cantidad)
        { 
            this.cantidad -= cantidad;
        }
    }
}
