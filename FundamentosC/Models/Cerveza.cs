﻿using _2_Clases_Objetos_Constructores.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FundamentosC.Models
{
    class Cerveza: Bebida, IBebidaAlcoholica
    {
        //public Cerveza() : base("Cerveza", 500)
        //{ 
                
        //}
        //Cerveza
        public Cerveza(int Cantidad, string Nombre = "Cerveza")
            :base(Nombre, Cantidad)
        { 
            
        }
        public int Id { get; set; }
        public String Marca { get; set; }
        public int Alcohol { get; set; }

        public void MaxRecomendado()
        {
            Console.WriteLine("Maximo permitido");
        }
    }
}
