﻿using _2_Clases_Objetos_Constructores.Models;
using FundamentosC.Models;
using MySqlConnector;
using System;

namespace Fundamentos
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Bebida bebida = new Bebida("CocaCola", 1000);
            //bebida.nombre = "Coca cola";
            //Console.WriteLine(bebida.nombre);
            //bebida.beberse(300);
            //Console.WriteLine(bebida.cantidad);

            //Cerveza cerveza = new Cerveza(600);
            //cerveza.beberse(50);
            //Console.WriteLine(cerveza.cantidad);

            //Array
            //int[] numeros = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            //List<int> lista = new List<int>();



            //for (int i = 0; i < numeros.Length; i++)
            //{
            //    lista.Add(i);
            //    Console.WriteLine(numeros[i]);
            //}
            //Console.WriteLine(lista.Count);

            //foreach (int i in lista)
            //{
            //    Console.WriteLine(i);
            //}

            //List<Cerveza> cervezas = new List<Cerveza>();
            //cervezas.Add(new Cerveza(500));
            //cervezas.Add(new Cerveza(1000));
            //cervezas.Add(new Cerveza(1500));
            //cervezas.Add(new Cerveza(2000));

            //foreach (Cerveza item in cervezas)
            //{
            //    Console.WriteLine("----------------------------------------");
            //    Console.WriteLine(item.cantidad);
            //}

            //var bebidaAlcoholeca = new Cerveza( 500);
            //MostrarRecomendacion(bebidaAlcoholeca);

            //String connectionString = "server=localhost;user=root;password=astres1992;database=data;";

            //MySqlConnection connection = new MySqlConnection(connectionString);

            //try
            //   {
            //       // Abre la conexión a la base de datos
            //       connection.Open();

            //   // Aquí puedes realizar operaciones en la base de datos



            //   // Cierra la conexión cuando hayas terminado
            //   connection.Close();
            //   }
            //   catch (Exception ex)
            //   {
            //       // Manejo de excepciones en caso de error
            //       Console.WriteLine("Error al conectar a la base de datos: " + ex.Message);
            //   }
            CervezaBD c = new CervezaBD();
            //insertar nueva cerveza
            //{ 
            //Cerveza cerveza = new Cerveza(15, "Mahou");
            //cerveza.Marca = "Roja Estrella";
            //cerveza.Alcohol = 5;

            //c.Add(cerveza);
            //}

            //{
            //    Cerveza cerveza = new Cerveza(15, "Mahou");
            //    cerveza.Marca = "Verde Estrella";
            //    cerveza.Alcohol = 5;

            //    c.Edit(cerveza, 5);
            //}

            {
                c.Delete(5);
            }
            var cervezas = c.Get();

            foreach (var item in cervezas)
            {
                Console.WriteLine(item.nombre);
            }
        }
        //    static void MostrarRecomendacion(IBebidaAlcoholica bebida)
        //    {
        //        bebida.MaxRecomendado();
        //    }

    }
}

    
